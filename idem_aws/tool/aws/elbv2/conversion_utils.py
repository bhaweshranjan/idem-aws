from collections import OrderedDict
from typing import Any
from typing import Dict

"""
Util functions to convert raw resource state from AWS ElasticLoadBalancingv2 Load Balancer to present input format.
"""


def convert_raw_load_balancer_to_present(
    hub,
    raw_resource: Dict[str, Any],
    idem_resource_name: str = None,
    tags: Dict[str, Any] = None,
    attributes: Dict[str, Any] = None,
) -> Dict[str, Any]:
    resource_id = raw_resource.get("LoadBalancerArn")
    resource_parameters = OrderedDict(
        {
            "SecurityGroups": "security_groups",
            "Scheme": "scheme",
            "Type": "lb_type",
            "IpAddressType": "ip_address_type",
            "CustomerOwnedIpv4Pool": "customer_owned_ipv4_pool",
        }
    )
    resource_translated = {"name": idem_resource_name, "resource_id": resource_id}
    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource:
            resource_translated[parameter_present] = raw_resource.get(parameter_raw)

    subnets = []
    subnet_mappings = []
    if raw_resource.get("AvailabilityZones"):
        for zone in raw_resource.get("AvailabilityZones"):
            subnets.append(zone.get("SubnetId"))
            if zone.get("LoadBalancerAddresses"):
                mapping_parameters = OrderedDict(
                    {
                        "SubnetId": zone.get("SubnetId"),
                        "AllocationId": zone.get("LoadBalancerAddresses")[0].get(
                            "AllocationId"
                        ),
                        "PrivateIPv4Address": zone.get("LoadBalancerAddresses")[0].get(
                            "PrivateIPv4Address"
                        ),
                        "IPv6Address": zone.get("LoadBalancerAddresses")[0].get(
                            "IPv6Address"
                        ),
                    }
                )
                mapping = {}
                for name, value in mapping_parameters.items():
                    if value:
                        mapping[name] = value
                if mapping:
                    subnet_mappings.append(mapping)
    resource_translated["subnets"] = subnets
    resource_translated["subnet_mappings"] = subnet_mappings

    if attributes:
        resource_translated["attributes"] = attributes
    if tags:
        resource_translated["tags"] = tags
    return resource_translated
