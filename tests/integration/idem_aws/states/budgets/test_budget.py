import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_budget(hub, ctx):
    # Skipping this as Budget is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return

    budget_name = "idem-test-" + str(uuid.uuid4())
    budget_limit = {"Amount": "1000.0", "Unit": "USD"}
    cost_filters = {"Service": ["Amazon API Gateway", "AmazonCloudWatch"]}
    cost_types = {
        "IncludeCredit": False,
        "IncludeDiscount": True,
        "IncludeOtherSubscription": True,
        "IncludeRecurring": True,
        "IncludeRefund": False,
        "IncludeSubscription": True,
        "IncludeSupport": True,
        "IncludeTax": True,
        "IncludeUpfront": True,
        "UseAmortized": False,
        "UseBlended": False,
    }
    time_unit = "MONTHLY"
    time_period = {
        "End": "2023-06-15",
        "Start": "2022-05-01",
    }
    budget_type = "COST"
    notifications_with_subscribers = [
        {
            "Notification": {
                "ComparisonOperator": "GREATER_THAN",
                "NotificationState": "OK",
                "NotificationType": "FORECASTED",
                "Threshold": 90.0,
            },
            "Subscribers": [
                {"Address": "ashishag@vmware.com", "SubscriptionType": "EMAIL"}
            ],
        }
    ]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create Budget with Test Flag
    ret = await hub.states.aws.budgets.budget.present(
        test_ctx,
        name=budget_name,
        budget_name=budget_name,
        time_unit=time_unit,
        budget_type=budget_type,
        budget_limit=budget_limit,
        cost_filters=cost_filters,
        cost_types=cost_types,
        time_period=time_period,
        notifications_with_subscribers=notifications_with_subscribers,
    )

    assert ret["result"], ret["comment"]
    assert f"Would create aws.budgets.budget '{budget_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert budget_name == resource.get("budget_name")
    assert time_unit == resource.get("time_unit")
    assert budget_type == resource.get("budget_type")
    assert budget_limit == resource.get("budget_limit")
    assert cost_filters == resource.get("cost_filters")
    assert cost_types == resource.get("cost_types")
    assert time_period == resource.get("time_period")
    assert notifications_with_subscribers == resource.get(
        "notifications_with_subscribers"
    )

    # Create Budget with real
    ret = await hub.states.aws.budgets.budget.present(
        ctx,
        name=budget_name,
        budget_name=budget_name,
        time_unit=time_unit,
        budget_type=budget_type,
        budget_limit=budget_limit,
        cost_filters=cost_filters,
        cost_types=cost_types,
        time_period=time_period,
        notifications_with_subscribers=notifications_with_subscribers,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.budgets.budget '{budget_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert budget_name == resource.get("budget_name")
    assert time_unit == resource.get("time_unit")
    assert budget_type == resource.get("budget_type")
    assert budget_limit == resource.get("budget_limit")
    assert cost_filters == resource.get("cost_filters")
    assert cost_types == resource.get("cost_types")
    assert time_period["Start"] in str(resource.get("time_period")["Start"])
    assert time_period["End"] in str(resource.get("time_period")["End"])
    assert notifications_with_subscribers == resource.get(
        "notifications_with_subscribers"
    )

    # Describe Budget
    describe_ret = await hub.states.aws.budgets.budget.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.budgets.budget.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.budgets.budget.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert resource_id == described_resource_map["name"]
    assert budget_name == described_resource_map.get("budget_name")
    assert time_unit == described_resource_map.get("time_unit")
    assert budget_type == described_resource_map.get("budget_type")
    assert budget_limit == described_resource_map.get("budget_limit")
    assert cost_filters == described_resource_map.get("cost_filters")
    assert cost_types == described_resource_map.get("cost_types")
    assert time_period["Start"] in str(
        described_resource_map.get("time_period")["Start"]
    )
    assert time_period["End"] in str(described_resource_map.get("time_period")["End"])
    assert notifications_with_subscribers == described_resource_map.get(
        "notifications_with_subscribers"
    )

    # Update Budget in test
    new_time_period = {
        "End": "2025-06-15",
        "Start": "2022-05-01",
    }
    new_budget_limit = {"Amount": "2000.0", "Unit": "USD"}
    new_time_unit = "QUARTERLY"
    ret = await hub.states.aws.budgets.budget.present(
        test_ctx,
        name=budget_name,
        resource_id=resource_id,
        budget_name=budget_name,
        time_unit=new_time_unit,
        budget_type=budget_type,
        budget_limit=new_budget_limit,
        cost_filters=cost_filters,
        cost_types=cost_types,
        time_period=new_time_period,
        notifications_with_subscribers=notifications_with_subscribers,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_time_unit == resource.get("time_unit")
    assert new_budget_limit == resource.get("budget_limit")
    assert new_time_period["Start"] in str(resource.get("time_period")["Start"])
    assert new_time_period["End"] in str(resource.get("time_period")["End"])

    # Update Budget in real
    ret = await hub.states.aws.budgets.budget.present(
        ctx,
        name=budget_name,
        resource_id=resource_id,
        budget_name=budget_name,
        time_unit=new_time_unit,
        budget_type=budget_type,
        budget_limit=new_budget_limit,
        cost_filters=cost_filters,
        cost_types=cost_types,
        time_period=new_time_period,
        notifications_with_subscribers=notifications_with_subscribers,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_time_unit == resource.get("time_unit")
    assert new_budget_limit == resource.get("budget_limit")
    assert new_time_period["Start"] in str(resource.get("time_period")["Start"])
    assert new_time_period["End"] in str(resource.get("time_period")["End"])

    # Delete Budget with test flag
    ret = await hub.states.aws.budgets.budget.absent(
        test_ctx, name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.budgets.budget '{budget_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert budget_name == resource.get("budget_name")
    assert new_time_unit == resource.get("time_unit")
    assert budget_type == resource.get("budget_type")
    assert new_budget_limit == resource.get("budget_limit")
    assert cost_filters == resource.get("cost_filters")
    assert new_time_period["Start"] in str(resource.get("time_period")["Start"])
    assert new_time_period["End"] in str(resource.get("time_period")["End"])
    assert notifications_with_subscribers == resource.get(
        "notifications_with_subscribers"
    )

    # Delete Budget in real
    ret = await hub.states.aws.budgets.budget.absent(
        ctx, name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.budgets.budget '{budget_name}'" in ret["comment"]
    resource = ret.get("old_state")
    assert budget_name == resource.get("budget_name")
    assert new_time_unit == resource.get("time_unit")
    assert budget_type == resource.get("budget_type")
    assert new_budget_limit == resource.get("budget_limit")
    assert cost_filters == resource.get("cost_filters")
    assert new_time_period["Start"] in str(resource.get("time_period")["Start"])
    assert new_time_period["End"] in str(resource.get("time_period")["End"])
    assert notifications_with_subscribers == resource.get(
        "notifications_with_subscribers"
    )

    # Delete Budget again
    ret = await hub.states.aws.budgets.budget.absent(
        ctx, name=budget_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.budgets.budget '{budget_name}' already absent" in ret["comment"]
