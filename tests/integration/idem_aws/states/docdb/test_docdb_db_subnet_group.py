import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NAME = "idem-test-docdb-" + str(int(time.time()))
PARAMETER = {
    "name": NAME,
    "db_subnet_group_description": "for testing idem plugin",
    "tags": {"Name": NAME},
}
comment_utils_kwargs = {
    "resource_type": "aws.docdb.db_subnet_group",
    "name": NAME,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup, aws_ec2_subnet, aws_ec2_subnet_2):
    global PARAMETER
    ctx["test"] = __test
    subnet_ids = [aws_ec2_subnet.get("SubnetId"), aws_ec2_subnet_2.get("SubnetId")]
    PARAMETER["subnet_ids"] = subnet_ids
    # Create DB Cluster with test flags
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    ret = await hub.states.aws.docdb.db_subnet_group.present(
        ctx,
        **PARAMETER,
    )
    if __test:
        assert would_create_message in ret["comment"]
    else:
        assert created_message in ret["comment"]
    assert ret["comment"], ret["result"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert PARAMETER["name"] == resource["name"]
    assert (
        PARAMETER["db_subnet_group_description"]
        == resource["db_subnet_group_description"]
    )
    assert PARAMETER["tags"] == resource["tags"]
    assert resource["subnet_ids"]
    assert len(resource["subnet_ids"]) == 2
    assert set(subnet_ids) == set(resource["subnet_ids"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.docdb.db_subnet_group.describe(ctx)
    resource_id = PARAMETER["name"]
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.docdb.db_subnet_group.present"))
    )
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource["tags"]
    assert (
        PARAMETER["db_subnet_group_description"]
        == resource["db_subnet_group_description"]
    )
    assert resource["subnet_ids"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update", depends=["describe", "present"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_tags = {
        f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
    }
    new_parameter["tags"] = new_tags
    new_parameter["resource_id"] = NAME
    would_update_message = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )[0]
    update_message = hub.tool.aws.comment_utils.update_comment(**comment_utils_kwargs)[
        0
    ]

    ret = await hub.states.aws.docdb.db_subnet_group.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    if __test:
        assert would_update_message in ret["comment"]
    else:
        assert update_message in ret["comment"]
    assert ret["new_state"]
    assert ret["old_state"]
    assert new_tags == ret["new_state"]["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["resource_id"] = NAME
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]
    ret = await hub.states.aws.docdb.db_subnet_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["name"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert would_delete_message in ret["comment"]
    else:
        assert deleted_message in ret["comment"]
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert (
        PARAMETER["db_subnet_group_description"]
        == resource["db_subnet_group_description"]
    )
    assert resource["subnet_ids"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="test_already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.docdb.db_subnet_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["name"]
    )
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert ret["result"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.docdb.db_subnet_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["name"]
        )
        assert (not ret["old_state"]) and (not ret["new_state"])
        assert ret["result"]
