import copy
import time
from collections import ChainMap

import pytest

STATE_NAME = "aws.s3.bucket_versioning"


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_bucket_versioning(hub, ctx, aws_s3_bucket):
    bucket_versioning_temp_name = "idem-test-bucket-versioning-" + str(int(time.time()))
    bucket = aws_s3_bucket.get("name")
    default_mfa_delete = "Disabled"
    status = "Suspended"
    new_status = "Enabled"

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create bucket versioning with test flag
    ret = await hub.states.aws.s3.bucket_versioning.present(
        test_ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        status=status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type=STATE_NAME, name=bucket_versioning_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_versioning_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert default_mfa_delete == resource.get("mfa_delete")
    assert status == resource.get("status")

    # Create bucket versioning
    ret = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        status=status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type=STATE_NAME, name=bucket_versioning_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_versioning_temp_name == resource.get("name")
    assert bucket == resource.get("resource_id")
    assert bucket == resource.get("bucket")
    assert default_mfa_delete == resource.get("mfa_delete")
    assert status == resource.get("status")

    resource_id = resource.get("resource_id")

    # Verify that the created bucket versioning is present (describe)
    ret = await hub.states.aws.s3.bucket_versioning.describe(ctx)

    resource_name = f"{resource_id}-versioning"
    assert resource_name in ret
    assert f"{STATE_NAME}.present" in ret.get(resource_name)
    resource = ret.get(resource_name).get(f"{STATE_NAME}.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_name == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert bucket == resource_map.get("bucket")
    assert default_mfa_delete == resource_map.get("mfa_delete")
    assert status == resource_map.get("status")

    # Create bucket versioning again with bucket name and resource_id different with test flag
    ret = await hub.states.aws.s3.bucket_versioning.present(
        test_ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=bucket_versioning_temp_name,
        status=status,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_versioning_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket versioning again with bucket name and resource_id different
    ret = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=bucket_versioning_temp_name,
        status=status,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_versioning_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket versioning again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.s3.bucket_versioning.present(
        test_ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=resource_id,
        status=status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_versioning_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create bucket versioning again with same resource_id and no change in state
    ret = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=resource_id,
        status=status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_versioning_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update bucket versioning with test flag
    ret = await hub.states.aws.s3.bucket_versioning.present(
        test_ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=resource_id,
        status=new_status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_versioning_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.would_update_comment(
            resource_type=STATE_NAME, name=bucket_versioning_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_versioning_temp_name == resource.get("name")
    assert bucket_versioning_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert default_mfa_delete == resource.get("mfa_delete")
    assert new_status == resource.get("status")

    # Update bucket versioning
    ret = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket,
        resource_id=resource_id,
        status=new_status,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_versioning_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.update_comment(
            resource_type=STATE_NAME, name=bucket_versioning_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_versioning_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert default_mfa_delete == resource.get("mfa_delete")
    assert new_status == resource.get("status")

    # Suspend bucket versioning with test flag
    ret = await hub.states.aws.s3.bucket_versioning.absent(
        test_ctx,
        name=bucket_versioning_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Would suspend {STATE_NAME} '{bucket_versioning_temp_name}'" in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret.get("old_state")
    assert bucket_versioning_temp_name == old_resource.get("name")
    assert bucket == old_resource.get("bucket")
    assert default_mfa_delete == old_resource.get("mfa_delete")
    assert new_status == old_resource.get("status")
    new_resource = ret.get("new_state")
    assert bucket_versioning_temp_name == new_resource.get("name")
    assert bucket == new_resource.get("bucket")
    assert default_mfa_delete == new_resource.get("mfa_delete")
    assert "Suspended" == new_resource.get("status")

    # Suspend bucket versioning
    ret = await hub.states.aws.s3.bucket_versioning.absent(
        ctx,
        name=bucket_versioning_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert f"{STATE_NAME} '{bucket_versioning_temp_name}' suspended" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    old_resource = ret.get("old_state")
    assert bucket_versioning_temp_name == old_resource.get("name")
    assert bucket == old_resource.get("bucket")
    assert default_mfa_delete == old_resource.get("mfa_delete")
    assert new_status == old_resource.get("status")
    new_resource = ret.get("new_state")
    assert bucket_versioning_temp_name == new_resource.get("name")
    assert bucket == new_resource.get("bucket")
    assert default_mfa_delete == new_resource.get("mfa_delete")
    assert "Suspended" == new_resource.get("status")

    # Suspend the same bucket versioning again (suspended state) will not invoke update on AWS side
    ret = await hub.states.aws.s3.bucket_versioning.absent(
        ctx,
        name=bucket_versioning_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"{STATE_NAME} '{bucket_versioning_temp_name}' already suspended"
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Suspend bucket versioning with no resource_id will consider it as absent
    ret = await hub.states.aws.s3.bucket_versioning.absent(
        ctx, name=bucket_versioning_temp_name
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=bucket_versioning_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
