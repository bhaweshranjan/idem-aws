"""
Test simple creation and deletion of an instance
"""
import time

import pytest

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(
    argnames="__test",
    argvalues=[True, False, True, False],
    ids=["--test", "run", "no change --test", "no change"],
)


@pytest.mark.fixture(scope="module", name="instance_name")
def temporary_instance_name():
    return "idem-fixture-instance-" + str(int(time.time()))


def setup_module():
    raise pytest.skip("The instance fixture is not yet functional")


INSTANCE_ID = None


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, aws_ec2_subnet, instance_name, __test):
    """
    Create a vanilla instance without using the instance fixture
    """
    global INSTANCE_ID
    instance_type = await hub.exec.aws.ec2.instance_type.get(
        ctx,
        name=instance_name,
        filters=[
            {"name": "instance-type", "values": ["*.nano"]},
            {"name": "hypervisor", "values": ["xen"]},
            {"name": "processor-info.supported-architecture", "values": ["x86_64"]},
        ],
    )

    # Search for an ami that exists in docker: https://docs.localstack.cloud/aws/elastic-compute-cloud/
    # For local tests, create a tag like this on an ubuntu:focal container:
    #     $ docker tag ubuntu:focal localstack-ec2/idem-test-ami:ami-000001
    image_id = await hub.states.aws.ec2.ami.search(
        ctx,
        name=None,
        most_recent=False,
        filters=[{"name": "name", "values": ["idem-test-ami"]}],
    )
    # Image from docker exists, use that for localstack
    if not image_id["result"]:
        # No image from docker exists, use any image we can find for real aws
        image_id = await hub.states.aws.ec2.ami.search(
            ctx,
            name=None,
            most_recent=False,
            filters=[
                {"name": "image-type", "values": ["machine"]},
                {"name": "state", "values": ["available"]},
                {"name": "hypervisor", "values": ["xen"]},
                {"name": "architecture", "values": ["x86_64"]},
            ],
        )

    if not image_id["result"]:
        raise pytest.skip("No image id found  for instance tests")

    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        instance_type=instance_type.ret.resource_id,
        image_id=image_id["new_state"]["resource_id"],
        tags={"Name": instance_name},
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    INSTANCE_ID = ret["new_state"]["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx):
    """
    Verify that "get" is successful after an instances has been created
    """
    ret = await hub.exec.aws.ec2.instance.get(ctx, resource_id=INSTANCE_ID)
    assert ret.result, ret.comment
    assert ret.ret["resource_id"] == INSTANCE_ID


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx):
    """
    Verify that "list" is successful after an instances has been created
    """
    ret = await hub.exec.aws.ec2.instance.list(
        ctx, filters=[{"Name": "instance-id", "Values": [INSTANCE_ID]}]
    )
    assert ret.result, ret.comment
    assert ret.ret
    # Verify that the created instance is in the list
    assert ret.ret[0]["resource_id"] == INSTANCE_ID


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, __test):
    """
    Destroy the instance created by the present state
    """
    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=INSTANCE_ID
    )
    assert ret["result"], ret["comment"]
    assert not ret["new_state"]

    if not __test:
        # Wait until terminated so the subnet doesn't have any leftover dependencies
        resource = hub.tool.boto3.resource.create(ctx, "ec2", "Instance", INSTANCE_ID)
        await hub.tool.boto3.resource.exec(resource, "wait_until_terminated")
