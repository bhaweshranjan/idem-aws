"""
Tests using the instance fixture to modify attributes
"""
import pytest
import yaml
from pytest_idem.runner import idem_cli
from pytest_idem.runner import run_yaml_block

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(
    argnames="__test",
    argvalues=[True, False, True, False],
    ids=["--test", "run", "no change --test", "no change"],
)


def setup_module():
    raise pytest.skip("The instance fixture is not yet functional")


@pytest.mark.localstack(pro=True)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_describe(acct_data, aws_ec2_instance, __test):
    """
    Describe all instances and run the "present" state the described instance created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    instance_id = aws_ec2_instance["resource_id"]

    # Describe all instances
    ret = idem_cli("describe", "aws.ec2.instance", acct_data=acct_data)

    # Get the state for our new instance, that was created in the fixture
    single_described_state = yaml.safe_dump({"described": ret[instance_id]})

    # Run the present state for our resource created by describe
    ret = run_yaml_block(
        single_described_state,
        acct_data=acct_data,
        test=__test,
    )
    tag = f"aws.ec2.instance_|-described_|-{instance_id}_|-present"
    assert tag in ret.keys()
    instance_ret = ret[tag]
    assert instance_ret["result"], instance_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert not instance_ret["changes"], instance_ret["changes"]


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_bootstrap_salt(aws_ec2_instance, __test):
    """
    Verify the ability to bootstrap an instance with salt and then remove salt
    """
