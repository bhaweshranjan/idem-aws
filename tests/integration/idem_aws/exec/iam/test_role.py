import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_iam_role):
    get_name = aws_iam_role["name"]
    ret = await hub.exec.aws.iam.role.get(ctx, name=get_name, resource_id=get_name)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert get_name == resource.get("name")
    assert aws_iam_role["arn"] == resource.get("arn")
    assert aws_iam_role["resource_id"] == resource.get("resource_id")
    assert aws_iam_role["path"] == resource.get("path")
    assert aws_iam_role["description"] == resource.get("description")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    get_name = "idem-test-exec-get-iam-" + str(int(time.time()))
    ret = await hub.exec.aws.iam.role.get(
        ctx,
        name=get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.iam.role '{get_name}' result is empty" in str(ret["comment"])
