import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get(hub, ctx, aws_elbv2_load_balancer):
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=aws_elbv2_load_balancer["name"],
        resource_id=aws_elbv2_load_balancer["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_elbv2_load_balancer["name"] == resource.get("name")
    assert aws_elbv2_load_balancer["resource_id"] == resource.get("resource_id")

    # get by name.
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=aws_elbv2_load_balancer["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_elbv2_load_balancer["name"] == resource.get("name")
    assert aws_elbv2_load_balancer["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    lb_get_name = "idem-test-exec-get-elbv2-lb-" + str(int(time.time()))
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=lb_get_name,
        resource_id="invalid-arn",
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeLoadBalancers operation: "
        f"'invalid-arn' is not a valid load balancer ARN" in str(ret["comment"])
    )

    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name="invalid-name-xyz-abc",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"LoadBalancerNotFoundException: An error occurred (LoadBalancerNotFound) when calling the DescribeLoadBalancers"
        f" operation: Load balancers '[invalid-name-xyz-abc]' not found"
        in str(ret["comment"])
    )
