# Find leftover Subnets
orphan_subnets:
  exec.run:
    - path: aws.ec2.subnet.list
    - kwargs:
        name: null
        filters:
          - name: "tag:Name"
            values:
              - "idem-fixture-subnet-*"

# Remove leftover Subnets
#!require:orphan_subnets
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_subnets}') %}

instance-{{ resource['resource_id'] }}:
  exec.run:
    - path: aws.ec2.instance.get
    - kwargs:
        name: null
        filters:
          - Name: subnet-id
            Values:
              - {{ resource['resource_id'] }}

# Remove instances that rely on test subnets
cleanup-instance-{{ resource['resource_id'] }}:
  aws.ec2.instance.absent:
    - resource_id: ${aws.ec2.instance:instance-{{ resource['resource_id'] }}:resource_id}

cleanup-{{ resource['resource_id'] }}:
  aws.ec2.subnet.absent:
    - resource_id: {{ resource['resource_id'] }}

{% endfor %}
#!END
