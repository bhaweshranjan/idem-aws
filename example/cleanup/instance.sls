# Find leftover Instances
orphan_instances:
  exec.run:
    - path: aws.ec2.instance.list
    - kwargs:
        name: null
        filters:
          - Name: "tag:Name"
            Values:
              - "idem-fixture-instance-*"

# Remove leftover Instances
#!require:orphan_instances
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_instances}') %}

cleanup-{{ resource['resource_id'] }}:
  aws.ec2.instance.absent:
    - resource_id: {{ resource['resource_id'] }}

{% endfor %}
#!END
